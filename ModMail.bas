Attribute VB_Name = "ModMail"
Public MailTo As String
Public MailFrom As String
Public MailSubject As String
Public MailBody As String
Public MailAttachments As String

Public MailSMTPServer As String
Public MailSMTPPort As Integer
Public MailSMTPUsername As String
Public MailSMTPPassword As String

Public MailSMTPSSL As Boolean

Public Attachments As New Collection
Public Function AddAttachments()
Dim Attachment
Dim I As Integer

If MailAttachments = vbNullString Then
    MsgBox "No attachements where set in the mailAttachments string", vbCritical
Else
    Attachment = Split(MailAttachments, ";")
    
    For I = 0 To UBound(Attachment)
        If Dir(Attachment(I), vbDirectory) = vbNullString Then
            MsgBox "Attachment can't be found: " & Attachment(I), vbCritical
        Else
            Attachments.Add Attachment(I)
        End If
    Next
End If
End Function
Public Function SendMail() As Boolean
On Error GoTo SendMail_Error:

Dim lobj_cdomsg As CDO.Message
Dim I As Integer

Set lobj_cdomsg = New CDO.Message

If MailSMTPServer = vbNullString Then: MsgBox "No SMTP server has been found, you must set a mail server", vbCritical, "SendMail": Exit Function
If MailTo = vbNullString Then: MsgBox "No MailTo Address has been set", vbCritical, "SendMail": Exit Function
If MailSubject = vbNullString Then: MsgBox "You must set a mail subject", vbCritical, "SendMail": Exit Function
If MailFrom = vbNullString Then: MsgBox "You must set a From Address", vbCritical, "SendMail": Exit Function

lobj_cdomsg.Configuration.Fields(cdoSMTPServer) = MailSMTPServer
lobj_cdomsg.Configuration.Fields(cdoSMTPServerPort) = MailSMTPPort
lobj_cdomsg.Configuration.Fields(cdoSMTPUseSSL) = False
lobj_cdomsg.Configuration.Fields(cdoSendUserName) = MailSMTPUsername
lobj_cdomsg.Configuration.Fields(cdoSendPassword) = MailSMTPPassword
lobj_cdomsg.Configuration.Fields(cdoSMTPConnectionTimeout) = 15
lobj_cdomsg.Configuration.Fields(cdoSendUsingMethod) = cdoSendUsingPort
lobj_cdomsg.Configuration.Fields.Update

lobj_cdomsg.To = MailTo
lobj_cdomsg.From = MailFrom
lobj_cdomsg.Subject = MailSubject
lobj_cdomsg.TextBody = MailBody

For I = 1 To Attachments.Count
    lobj_cdomsg.AddAttachment Attachments(I)
Next

lobj_cdomsg.Send

Set lobj_cdomsg = Nothing

SendMail = True
Exit Function
          
SendMail_Error:
    MsgBox (Err.Description), vbCritical
End Function
