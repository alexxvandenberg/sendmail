Attribute VB_Name = "ModMain"
Sub Main()
Dim SplitValue
Dim SplitValueCommand

Dim CommandValue As String

Dim I As Integer

If Command$ = vbNullString Then
    MsgBox "Command line SMTP mailer Version " & App.Minor & "." & App.Major & "." & App.Revision & vbNewLine & _
    "Copyright (C) 2017 Sibra-Soft (Alex van den Berg)" & vbNewLine & vbNewLine & _
    "Usage: sendmail.exe [Options]" & vbNewLine & _
    "/T" & vbTab & "To address (address of the receiver)" & vbNewLine & _
    "/F" & vbTab & "From address (address of the sender)" & vbNewLine & _
    "/A" & vbTab & "Mail subject" & vbNewLine & _
    "/B" & vbTab & "Mail message body text" & vbNewLine & _
    "/S" & vbTab & "SMTP server name" & vbNewLine & _
    "/O" & vbTab & "SMTP server port" & vbNewLine & _
    "/U" & vbTab & "SMTP server username" & vbNewLine & _
    "/P" & vbTab & "SMTP server password (You must set a username)" & vbNewLine & _
    "/M" & vbTab & "Mail attachements (seperator = ;)" & vbNewLine & _
    "/H" & vbTab & "Mail content opmaken als HTML"
    
Else
    SplitValue = Split(Command$, "/")
    
    For I = 1 To UBound(SplitValue)
        SplitValueCommand = Split(SplitValue(I), "=")
        
        CommandValue = Trim(SplitValueCommand(1))
        
        Select Case SplitValueCommand(0)
            Case "T": ModMail.MailTo = CommandValue
            Case "F": ModMail.MailFrom = CommandValue
            Case "A": ModMail.MailSubject = CommandValue
            Case "B": ModMail.MailBody = CommandValue
            Case "S": ModMail.MailSMTPServer = CommandValue
            Case "O": ModMail.MailSMTPPort = CommandValue
            Case "U": ModMail.MailSMTPUsername = CommandValue
            Case "P": ModMail.MailSMTPPassword = CommandValue
            Case "M": ModMail.MailAttachments = CommandValue: ModMail.AddAttachments
            
        End Select
    Next
    
    If ModMail.SendMail = False Then
        MsgBox "Mail can't be send", vbCritical
    End If
End If
End Sub
